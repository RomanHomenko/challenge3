//
//  ViewController.swift
//  Challenge3
//
//  Created by Роман Хоменко on 04.04.2022.
//

import UIKit


// MARK: - LoadView
extension ViewController {
    override func loadView() {
        view = UIView()
        view.backgroundColor = .white
        
        remainingAttemptsLabel = UILabel()
        remainingAttemptsLabel.translatesAutoresizingMaskIntoConstraints = false
        remainingAttemptsLabel.font = UIFont.systemFont(ofSize: 18)
        remainingAttemptsLabel.text = "Remaining attempts: 0"
        view.addSubview(remainingAttemptsLabel)
        
        clueLabel = UILabel()
        clueLabel.translatesAutoresizingMaskIntoConstraints = false
        clueLabel.font = UIFont.systemFont(ofSize: 24)
        clueLabel.text = "PLACEHOLDER"
        view.addSubview(clueLabel)
        
        currentAnswer = UITextField()
        currentAnswer.translatesAutoresizingMaskIntoConstraints = false
        currentAnswer.placeholder = "Some word here..."
        currentAnswer.textAlignment = .center
        currentAnswer.font = UIFont.systemFont(ofSize: 24)
        currentAnswer.addTarget(self, action: #selector(changeTextField), for: .editingChanged)
        view.addSubview(currentAnswer)
        
        submitButton = UIButton(type: .system)
        submitButton.translatesAutoresizingMaskIntoConstraints = false
        submitButton.setTitle("Submit", for: .normal)
        submitButton.addTarget(self, action: #selector(submitTapped), for: .touchUpInside)
        view.addSubview(submitButton)
        
        clearButton = UIButton(type: .system)
        clearButton.translatesAutoresizingMaskIntoConstraints = false
        clearButton.setTitle("Clear", for: .normal)
        clearButton.addTarget(self, action: #selector(clearTapped), for: .touchUpInside)
        view.addSubview(clearButton)
        
        refreshWordButton = UIButton(type: .system)
        refreshWordButton.translatesAutoresizingMaskIntoConstraints = false
        refreshWordButton.setTitle("Refresh", for: .normal)
        refreshWordButton.addTarget(self, action: #selector(refreshTapped), for: .touchUpInside)
        view.addSubview(refreshWordButton)
        
        NSLayoutConstraint.activate([
            refreshWordButton.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor),
            refreshWordButton.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
            
            remainingAttemptsLabel.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor),
            remainingAttemptsLabel.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor),
            
            clueLabel.topAnchor.constraint(equalTo: remainingAttemptsLabel.bottomAnchor, constant: 18),
            clueLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            currentAnswer.topAnchor.constraint(equalTo: clueLabel.bottomAnchor, constant: 24),
            currentAnswer.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            submitButton.topAnchor.constraint(equalTo: currentAnswer.bottomAnchor, constant: 36),
            submitButton.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor),
            submitButton.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: +100),
            
            clearButton.topAnchor.constraint(equalTo: currentAnswer.bottomAnchor, constant: 36),
            clearButton.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
            clearButton.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: -100),
            
            
        ])
    }
}

// MARK: - ViewDidload
class ViewController: UIViewController {
    var clueLabel: UILabel!
    var remainingAttemptsLabel: UILabel!
    
    var currentAnswer: UITextField!
    
    var submitButton: UIButton!
    var clearButton: UIButton!
    var refreshWordButton: UIButton!
    
    var filledCharacters: [Character] = []
    var solution: [Character] = []
    
    var attempts: Int = 0
    let loseGameMessage: String = "You lose, try again!"
    let winGameMessage: String = "You win! Another try?"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchWordURL()
    }
}

// MARK: - @objc funcs for buttons' actions
extension ViewController {
    @objc func changeTextField(_ textField: UITextField) {
        if let answer = textField.text {
            if answer.count > 1 || answer.count == 0 {
                submitButton.isEnabled = false
            } else {
                submitButton.isEnabled = true
            }
        }
    }
    
    @objc func submitTapped(_ sender: UIButton) {
        guard let answerCharacter = currentAnswer.text?.capitalized else { return }
        
        attempts += 1
        
        for (index, character) in solution.enumerated() {
            if character == Character(answerCharacter) {

                filledCharacters[index] = character
                clueLabel.text! = String(filledCharacters)
            }
        }
        
        if !filledCharacters.contains("@") {
            showAlert(with: winGameMessage)
        }
        if (solution.count + 3) - attempts == 0 {
            showAlert(with: loseGameMessage)
        }
        
        currentAnswer.text = ""
        remainingAttemptsLabel.text = "Remaining attempts: \((solution.count + 3) - attempts)"
    }
    
    @objc func refreshTapped(_ sender: UIButton) {
        fetchWordURL()
        loadGame()
    }
    
    @objc func clearTapped(_ sender: UIButton) {
        loadGame()
    }
}

// MARK: - game logic funcs
extension ViewController {
    func loadGame() {
        filledCharacters = []
        attempts = 0
        
        for _ in 0..<solution.count {
            filledCharacters.append("@")
        }
        
        clueLabel.text = String(filledCharacters)
        remainingAttemptsLabel.text = "Remaining attempts: \((solution.count + 3) - attempts)"
    }
}

// MARK: - Fetch words from bundle func
extension ViewController {
    func fetchWordURL() {
        if let wordsFileURL = Bundle.main.url(forResource: "words", withExtension: "txt") {
            if let wordsContent = try? String(contentsOf: wordsFileURL) {
                var words = wordsContent.components(separatedBy: "\n")
                words.removeLast() // last element is empty string
                words.shuffle()
                
                solution = words[0].map { Character(extendedGraphemeClusterLiteral: $0) }
                
                loadGame()
            }
        }
    }
}

// MARK: - create alert after win or losee
extension ViewController {
    func showAlert(with message: String) {
        
        let alert = UIAlertController(title: message, message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default) { [weak self] _ in
            self?.fetchWordURL()
        })
        
        present(alert, animated: true)
    }
}
